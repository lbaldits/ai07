#Analyse des données

* Étude des sources de données (quantification, analyses générales)

    *Fichier des ventes*
    
    *  Fantastic, serveur web de l'entreprise (http://pic.crzt.fr/ai07/fantastic), 512018 lignes
        *  Ticket number (INTEGER)
            *  Description : numéro du ticket de vente 
            *  Qualité : 2
            *  Utilité : 3
            *  Commentaires : il y a quelques erreurs au niveau des numéros des tickets (égal à 0 plusieurs fois).
        *  Date (DATE)
            *  Description : Date de création du ticket
            *  Qualité : 2
            *  Utilité : 2
            *  Commentaires : Certaines lignes n'ont pas de dates
        *  ISBN (CHAR(13))
            *  Description : International Standard Book Number
            *  Qualité : 2
            *  Utilité : 3
            *  Commentaires : Certaines ISBN sont vides et d'autres ont un format non correct (3 caractères au lieu de 13)
        *  Store (VARCHAR(255))
            *  Description : magasin de la transaction
            *  Qualité : 3
            *  Utilité : 2
    
        
    *Fichier des magasins*
    
    *  marketing.ods service marketing de l'entreprise (http://pic.crzt.fr/ai07/fantastic/), 153 lignes
        *  Magasin (VARCHAR(255))
            *  Description : Numéro de référence du magasin
            *  Qualité : 3
            *  Utilité : 3
        *  Département (INTEGER)
            *  Description : Département dans lequel se trouve le magasin
            *  Qualité : 3
            *  Utilité : 2
            *  Commentaires :
        *  Rayonnage (TYPE ENUM {E,A,Y})
            *  Description : Type de tri des livres dans les rayonnages du magasin
            *  Qualité : 3
            *  Utilité : 2
        *  Rayon Bestseller (TYPE ENUM {0,1})
            *  Description : Existence ou non d'un rayon bestsellers
            *  Qualité : 3
            *  Utilité : 1
            *  Commentaires :
        *  Rayon Recent (TYPE ENUM{0,1})
            *  Description : Existence ou non d'un rayon livres récents
            *  Qualité : 3
            *  Utilité : 1
            *  Commentaires :
    
    
    
    *Catalogue des livres*
    
    * catalogue.csv, export CSV depuis le serveur oracle (sme-oracle.sme.utc), 1444 lignes
        * REF (INTEGER)
            *  Description : Numéro de Référence du livre
            *  Qualité : 3
            *  Utilité : 1
            *  Commentaires : Colonne qui ne sert à rien, à cause de l'ISBN
        * ISBN (CHAR(13))
            *  Description : Identifiant du livre 
            *  Qualité : 2
            *  Utilité : 3
            *  Commentaires : Beaucoup de lignes ont une ISBN qui ne respecte la norme de 13 caractères
        * TITLE (VARCHAR(255))
            *  Description : International Standard Book Number
            *  Qualité : 2
            *  Utilité : 1
        * AUTHORS (VARCHAR(255))
            *  Description : Auteur(s) du livre
            *  Qualité : 3
            *  Utilité : 2
        * LANGUAGE (CHAR(3))
            *  Description : Langage du livre
            *  Qualité : 3
            *  Utilité : 2
        * PUBDATE (DATE)
            *  Description : Date de publication
            *  Qualité : 2
            *  Utilité : 3
        * PUBLISHER (VARCHAR(255))
            *  Description : Editeur du livre
            *  Qualité : 0
            *  Utilité : 2
            *  Commentaires : Presque toutes les lignes sont égales à '?'
        * TAGS (VARCHAR(255))
            *  Description : Tag pour compléter le genre du livre
            *  Qualité : 2
            *  Utilité : 1
            *  Commentaires : Colonne qui se répète parfois avec la colonne du genre
        * GENRE (VARCHAR(255))
            *  Description : Genre du livre
            *  Qualité : 2
            *  Utilité : 2
    
    
    *Départements INSEE 2013*
    
    * departementsInsee2003.txt, serveur web de l'entreprise (http://pic.crzt.fr/ai07/fantastic), 95 lignes
        * Department (INTEGER)
            *  Description : Numéro de département
            *  Qualité : 3
            *  Utilité : 3
        * DptName (VARCHAR(255))
            *  Description : Nom du département
            *  Qualité : 2
            *  Utilité : 1
            *  Commentaires : Il y a quelques problèmes d'encodage des caractères 
        * Population (INTEGER)
            *  Description : Nombre d'habitants du département
            *  Qualité : 3
            *  Utilité : 3


* Modèle Relationnel - 3NF

    * Vente(#TicketNumber (INTEGER), Date (DATE), ISBN (CHAR(13)) => Catalogue, Store (VARCHAR(255)) => Magasin)
    * Magasin(#Id (VARCHAR(255)), Departement (INTEGER) => Departement, Rayonnage [E,A,Y]), Bestseller (Boolean), Recent (Boolean))
    * Catalogue(REF (INTEGER), #ISBN (CHAR(13)), TITLE (VARCHAR(255)), AUTHORS (VARCHAR(255)), LANGAGE (CHAR(3)), PUBDATE (DATE), PUBLISHER (VARCHAR(255)), TAGS (VARCHAR(255)), GENRE (VARCHAR(255))) avec (REF,ISBN,TITLE,AUTHORS) NOT NULL
    * Departement(#Numero (INTEGER), DptName (VARCHAR(255)), Population (INTEGER))
    
