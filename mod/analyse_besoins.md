#Analyse des besoins

* Exprimer les besoins sous la forme de requêtes décisionnelles

    **Question 1**
    
    La direction marketing est en charge de l’implantation des magasins dans les départements et de l'organisation des rayonnages (type de rangement et présence de rayons spécifiques pour les best-sellers). Elle cherche à savoir si l'organisation du rayonnage des magasins a une influence sur les volumes ventes, et si cela varie en fonction des jours de la semaine ou de certaines périodes de l'année. Elle voudrait également savoir si certains magasins ou départements sont plus dynamiques que d'autres.
    
    Requête décisionnelle :
    
        1.Quantité de livres vendus
        2./Magasin
        3./Ordre de Rayonnage
        4./Existence du Rayon BestSeller
        5./Jour de la semaine
        6./Mois
        7./Département
     **Question 2**
     
     La direction éditoriale se demande si certains livres se vendent mieux à certaines dates et/ou dans certains magasins ou départements. Elle aimerait également savoir si certains auteurs ou éditeurs se vendent mieux, et s'il existe un lien entre l'ancienneté des livres et les ventes. Elle se demande aussi si certaines périodes sont plus propices que d'autres à l'écoulement des livres les plus anciens.
     
     Requête décisionnelle :
     
        1.Nombres de copies vendues
        2./Livre
        3./Date
        4./Magasin
        5./Département
        6./Auteurs
        7./Editeurs
        5./Âge
        6./Mois
        7./Trismestre
    
* Réaliser les vues hiérarchiques pour chaque requête 

    **Question 1**
    
     Vue Hiérarchique :
    
        1.Quantité de livres vendus
        2./Lieu (Magasin, Département)
        3./Ordre de Rayonnage
        4./Existence du Rayon BestSeller
        5./Date (Jour de la semaine, Mois, trimestre, semestre)
    
    **Question 2**
    
     Vue Hiérarchique :
     
        1.Nombres de copies vendues
        2./Livre
        3./Mesure (Date) (Magasin, Département)
        4./Personne (Auteurs, Editeurs)
        5./Âge
        6./Date (Jour de la semaine, mois, trismestre, semestre)