--Fichier de création des tabels de faits et dimensions à partir de la zone T

CREATE OR REPLACE FUNCTION checkVarchar(toCheck VARCHAR) RETURNS VARCHAR AS
$$
BEGIN
    IF toCheck='?' OR toCheck='' OR toCheck SIMILAR TO ' *' THEN
      RETURN NULL;
    ELSE
      RETURN toCheck;
    END IF;
END;
$$ LANGUAGE plpgsql;

CREATE OR REPLACE FUNCTION checkBigInt(ref text) RETURNS BIGINT AS
$$
BEGIN
    IF ref NOT SIMILAR TO '[\d]*' OR ref='' OR ref SIMILAR TO ' *' THEN
      RETURN NULL;
    ELSE
      RETURN CAST(ref as BIGINT);
    END IF;
END;
$$ LANGUAGE plpgsql;

CREATE OR REPLACE FUNCTION checkDate(entryDate IN text) RETURNS DATE AS
$$
BEGIN
  IF entryDate SIMILAR TO '[\d]{4}-[\d]{2}-[\d]{2}' THEN
      return TO_DATE(entryDate, 'YYYY-MM-DD');
  ELSIF entryDate SIMILAR TO '[\d]{4}-[\d]{2}-[\d]{2}T[\d]{2}:[\d]{2}:[\d]{2}\+[\d]{2}:[\d]{2}' THEN
      return TO_DATE(entryDate, 'YYYY-MM-DD');
  ELSE
      return NULL;
  END IF;
END;
$$ LANGUAGE plpgsql;

CREATE OR REPLACE FUNCTION checkDept(dept VARCHAR(3)) RETURNS CHAR(3) AS
$$
BEGIN
  IF dept SIMILAR TO '(\d){1,3}' THEN
      IF CAST (dept AS INTEGER)<10 THEN
        return ('0'||dept);
      ELSE
        return dept;
      END IF;
  ELSE
      return NULL;
  END IF;
END;
$$ LANGUAGE plpgsql;

CREATE OR REPLACE FUNCTION checkMag(store VARCHAR(4)) RETURNS CHAR(4) AS
$$
BEGIN
  IF store SIMILAR TO 'M(\d)*' THEN
      return store;
  ELSE
      return NULL;
  END IF;
END;
$$ LANGUAGE plpgsql;

CREATE OR REPLACE FUNCTION checkRayon(rayon char(1)) RETURNS CHAR(1) AS
$$
BEGIN
  IF Rayon='A' OR Rayon='E' OR Rayon='Y' THEN
      return Rayon;
  ELSE
      return NULL;
  END IF;
END;
$$ LANGUAGE plpgsql;

SET AUTOCOMMIT = ON;

-- CREATE TABLE Dates AS SELECT --créer la table dates pour stockage statique
DROP TABLE IF EXISTS vDates CASCADE;
CREATE TABLE vDates(CLEF,Date_sold,JDS,JDA,Mois,Trimestre,Semaine)
AS
  SELECT DISTINCT
  CASE WHEN checkDate(T.FANTASTIC.DATE_SOLD) IS NOT NULL
    THEN RANK() OVER (ORDER BY T.FANTASTIC.DATE_SOLD)
    ELSE 0
  END,
  checkDate(T.FANTASTIC.DATE_SOLD),
  TO_CHAR(checkDate(T.FANTASTIC.DATE_SOLD),'DAY') AS JDS,
  TO_CHAR(checkDate(T.FANTASTIC.DATE_SOLD),'DDD') AS JDA,
  TO_CHAR(checkDate(T.FANTASTIC.DATE_SOLD),'MM') AS MOIS,
  TO_CHAR(checkDate(T.FANTASTIC.DATE_SOLD),'Q') AS TRIMESTRE,
  TO_CHAR(checkDate(T.FANTASTIC.DATE_SOLD),'IW') AS SEMAINE
  FROM T.FANTASTIC;

DROP TABLE IF EXISTS vMagasin CASCADE;
CREATE TABLE vMagasin(MAGASIN,CLEF,DEPT,RAYONNAGE,BS,RECENT,POP)
AS
  SELECT DISTINCT
  checkMag(T.MARKETING.MAGASIN),
  CASE WHEN checkMag(T.MARKETING.MAGASIN) IS NOT NULL
    THEN RANK() OVER (ORDER BY T.MARKETING.MAGASIN)
    ELSE 0
  END,
  checkDept(T.MARKETING.DEPT),
  checkRayon(T.MARKETING.RAYONNAGE),
  T.MARKETING.RAYON_BS,
  T.MARKETING.RAYON_RECENT,
  checkInt(T.DEPTINSEE2003.POP)
  FROM T.MARKETING INNER JOIN T.DEPTINSEE2003 ON CAST(checkDept(T.MARKETING.DEPT) AS INTEGER) = CAST(checkDept(T.DEPTINSEE2003.DEPT_NUM) AS INTEGER);

--Avec un SELECT COUNT(DISTINCT T.CATALOGUE.ISBN) FROM T.Catalogue; on sait que ISBN peut servir de clef primaire dans Produit

DROP TABLE IF EXISTS vProduit CASCADE;
CREATE TABLE vProduit(ISBN,CLEF,Ref,Titre,Langue,Auteur,Editeur,Age,Genre)
AS
  SELECT DISTINCT
  checkVarchar(T.CATALOGUE.ISBN),
  CASE WHEN checkVarchar(T.CATALOGUE.ISBN) IS NOT NULL
    THEN RANK() OVER (ORDER BY T.CATALOGUE.ISBN)
    ELSE 0
  END,
  checkVarchar(T.CATALOGUE.REF),
  checkVarchar(T.CATALOGUE.TITLE),
  checkVarchar(T.CATALOGUE.LANGUAGE),
  checkVarchar(T.CATALOGUE.AUTHORS),
  checkVarchar(T.CATALOGUE.PUBLISHER),
  CAST(DATE_PART('year',current_date)-DATE_PART('year',checkDate(T.CATALOGUE.PUBDATE)) AS INTEGER),
  checkVarchar(T.CATALOGUE.GENRE)
  FROM T.CATALOGUE
  WHERE checkVarchar(T.CATALOGUE.ISBN) IS NOT NULL;

DROP TABLE IF EXISTS vFantastic CASCADE;
CREATE TABLE vFantastic(TICKET_NUMBER, DATE_SOLD, ISBN, STORE)
AS
  SELECT
    checkBigInt(TICKET_NUMBER),
    checkDate(DATE_SOLD),
    ISBN,
    STORE
  FROM T.FANTASTIC;

CREATE INDEX I1 ON vFantastic (TICKET_NUMBER);
CREATE INDEX I2 ON vFantastic (DATE_SOLD);
CREATE INDEX I3 ON vFantastic (ISBN);
CREATE INDEX I4 ON vFantastic (STORE);
CREATE UNIQUE INDEX I5 ON vMagasin (Magasin);
CREATE UNIQUE INDEX I6 ON vProduit (ISBN);
CREATE UNIQUE INDEX I7 ON vDates (Date_sold);


--Un problème de performances nous empêche ici d'avoir les dates dans la table de faits.
  -- CREATE OR REPLACE VIEW vVentes(fkProduit,fkMagasin,TICKET_NUMBER)
  -- AS
  --   SELECT
  --   vProduit.CLEF,
  --   vMagasin.CLEF,
  --   checkBigInt(T.FANTASTIC.TICKET_NUMBER)
  --   FROM  T.FANTASTIC
  --         INNER JOIN vProduit ON vProduit.ISBN = T.FANTASTIC.ISBN
  --         INNER JOIN vMagasin ON vMagasin.Magasin = T.FANTASTIC.STORE;

DROP TABLE IF EXISTS vVentes CASCADE;
CREATE TABLE vVentes(fkDate,fkProduit,fkMagasin,TICKET_NUMBER)
AS
  SELECT
  vDates.CLEF,
  vProduit.CLEF,
  vMagasin.CLEF,
  vFantastic.TICKET_NUMBER
  FROM  vFantastic LEFT JOIN vDates ON vFantastic.DATE_SOLD = vDates.Date_sold
        LEFT JOIN vProduit ON vFantastic.ISBN = vProduit.ISBN
        LEFT JOIN vMagasin ON vFantastic.STORE = vMagasin.Magasin;
