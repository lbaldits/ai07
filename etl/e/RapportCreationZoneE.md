**Rapport de Création de la zone E et tests réalisés à la main**

* Nous avons tout d'abord créé un schéma ETL avec la requête : CREATE SCHEMA ETL

* Puis, il nous a fallu exécuter le fichier .sql de création de la zone E.
  \i '/volsme/users/bdd1p012/Documents/AI07/creationZoneE.sql'

* La commande terminal \dt nous permet de vérifier que les tables ont bien été créées. 
* Nous accordons les droits nécessaires à l'utilisateur : BDD1P199 

    `GRANT ALL ON ALL TABLES IN SCHEMA ETL TO BDD1P199`

* Puis, après récupérer les fichiers en format CSV depuis les différents serveurs, nous pouvons les importer à la main dans notre base de données la commande \copy étant alors celle utilisée :

  * Import du fichier departementsInsee2003.txt :
 
        \copy DEPTINSEE2013 FROM '/volsme/users/bdd1p012/Documents/AI07/departementsInsee2003.txt' DELIMITER ';' CSV;
        Réponse du serveur PostgreSQL : COPY 95

  * Import du fichier Fantastic :
 
        \copy FANTASTIC FROM '/volsme/users/bdd1p012/Documents/AI07/Fantastic' DELIMITER ';' CSV;
        Réponse du serveur PostgreSQL : COPY 512018

  * Import du fichier marketing.ods :

        Il est nécessaire ici de passer par un fichier CSV. Nous avons utilisé la fonction de LibreOffice permettant de faire la transformation, avec pour deLIMITer ';' et en mettant tous les textes entre guillemets.
        \copy MARKETING FROM '/volsme/users/bdd1p012/Documents/AI07/marketing.csv' DELIMITER ';' CSV HEADER;
        Réponse du serveur PostgreSQL : COPY 154
        L'argument HEADER nous permet ici de ne pas importer la première ligne.

  * Import des données situées sur le server Oracle :
      
        Export des données de la Table Catalogue depuis le Client Oracle, en format CSV, grâce à une fonction du client.
        \copy CATALOGUE FROM '/volsme/users/bdd1p012/Documents/AI07/CATALOGUE_DATA_TABLE.csv' DELIMITER ',' CSV HEADER;
        Réponse du serveur PostgreSQL : COPY 1443


    ___VERIFICATIONS___

* On vérifie ici le nombre de lignes par table. Les résultats correspondent bien.

    `SELECT COUNT(*) FROM marketing; 154`
    
    `SELECT COUNT(*) FROM fantastic; 512018`
    
    `SELECT COUNT(*) FROM catalogue; 1443`
    
    `SELECT COUNT(*) FROM DEPTINSEE2013; 95`

* On va ensuite faire une vérification aléatoire des données :

    `SELECT * FROM MARKETING WHERE RANDOM() < 0.1 LIMIT 10;`
    
    Avec le résultat que cette requête renvoie, i.e., quelques lignes aléatoirement sélectionnées, il suffit de se reporter au fichier en lui-même

    On fera de même avec les requêtes suivantes :
    
    `SELECT * FROM DEPTINSEE2013 WHERE RANDOM() < 0.1 LIMIT 8;`
    
    `SELECT * FROM FANTASTIC WHERE RANDOM() < 0.1 LIMIT 50;`
    
    `SELECT * FROM CATALOGUE WHERE RANDOM() < 0.1 LIMIT 15;`

    Grâce à la fonctin LIMIT, il nous est possible de modifier le nombre de lignes désirées par sélection aléatoire en fonction du nombre total de lignes de la table.
    En effet, il peut être plus pertinent de regarder 50 lignes d'une seule requête lorsque la table contient 512018 lignes.

* Les header des fichiers Marketing et Catalogue ont été enlevés durant la requête copy. Néanmoins, dans le fichier marketing, une annotation en fin de page est restée, ce qui nous donne une ligne vide et une ligne non conforme en toute fin de table. En effet, la requête SELECT * FROM MARKETING WHERE magasin IS NULL; renvoie 1 ligne.
    
    La requête SELECT * FROM MARKETING WHERE DEPT IS NULL; nous renvoie les 2 lignes faussées.
