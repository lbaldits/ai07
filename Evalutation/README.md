#Contenu du dossier

Ce dossier contient les scripts et modélisations du datawarehouse pour le projet AI07 DuneCorp(c)

    * ModeleEtoile.plantuml
        Ce fichier contient la modélisation dimenstionnelle finale du DW
    * DF.plantuml
        Ce fichier contient les dépendances fonctionnelles allant avec la modélisation dimensionnelle
    * scriptDuneCorp.sql
        Ce fichier contient un script de création de bout en bout de l'ETL correspondant. Les données sont aussi importées ici. A la fin de l'exécution du fichier sur le serveur PostgreSQL, le DW est utilisable pour effectuer une analyse des données. 
    * scriptETL.sh
        Ce fichier contient un script automatisant le téléchargements des données depuis le serveur, le création de l'ETL et l'import des données.
        Ce fichier a été oublié durant le rendu final de l'ETL.
    * Logs.txt
        Ce fichier contient le journal de logs créé par la création de l'ETL.
    * Requetes.sql
        Ce fichier contient les requêtes permettant de répondre aux demande de la direction commerciale.
    * requetes.sh
        Ce fichier automatise la récupération des résultats des requêtes.
    * requetes.txt
        Ce fichier contient les données brutes des résultats des requêtes.
    * Rapport.pdf
        Ce rapport contient l'analyse des résultats (SQL, Graphiques,données brutes) des questions données par la direction commerciale.
    * Rapport2.pdf
        Ce rapport contient l'analyse des résultats (SQL, Graphiques, données brutes) de la 2ème partie des questions posées.
        

Le dossier exportCSV contient les données utilisées pour l'analyse des résultats du premier rapport. Les résultats des requêtes sont déjà en données brutes dans le rapport mais nous sommes passés par l'export CSV pour les graphiques.

#Pour l'utilisation des scripts sh

Dans un répertoire contenant scriptDuneCorp.sql et requetes.sql

Pour lancer le script initialisant le téléchargement des données, la création de l'ETL, l'insertion des données 
et donnant un fichier de logs, il suffit d'avoir le script dans un répertoire et de le lancer dans un terminal :

./scriptETL.sh > Logs.txt 


Le journal de création/insertion se trouve dans le fichier Logs.txt du répertoire courant.

De même, pour lancer les requêtes répondant à la demande de la direction commerciale, il suffit de lancer
la commande suivant dans le répertoire courant :

./requetes.sh > requetes.txt 






